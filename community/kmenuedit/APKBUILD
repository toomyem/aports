# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kmenuedit
pkgver=5.25.0
pkgrel=0
pkgdesc="KDE menu editor"
# armhf blocked by qt5-qtdeclarative
# s390x blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kdbusaddons-dev
	kdoctools-dev
	kglobalaccel-dev
	ki18n-dev
	kiconthemes-dev
	kinit-dev
	kio-dev
	kitemviews-dev
	kxmlgui-dev
	qt5-qtbase-dev
	samurai
	sonnet-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/kmenuedit-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
e8b146889a2179177c5acfeced50f3a2e25318893eec7a700015ea550ffa9158e09be5d9c45c1b60d4c24534029570b79d417b69821cc7635123544d18921058  kmenuedit-5.25.0.tar.xz
"
