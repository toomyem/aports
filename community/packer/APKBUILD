# Maintainer: Galen Abell <galen@galenabell.com>
# Contributor: Galen Abell <galen@galenabell.com>
pkgname=packer
pkgver=1.8.1
pkgrel=0
pkgdesc="tool for creating machine images for multiple platforms"
url="https://www.packer.io/"
license="MPL-2.0"
arch="all"
makedepends="go"
options="net chmod-clean"
source="$pkgname-$pkgver.tar.gz::https://github.com/hashicorp/packer/archive/v$pkgver.tar.gz"

export GOPATH="$srcdir"

build() {
	export GOFLAGS="$GOFLAGS -trimpath"
	export GOLDFLAGS="-s -w -X main.GitCommit=v$pkgver"
	go build -v -o bin/$pkgname
}

check() {
	go list . | xargs -t -n4 go test -timeout=2m -parallel=4
	bin/$pkgname -v
}

package() {
	install -Dm755 bin/"$pkgname" -t "$pkgdir"/usr/bin/
}

sha512sums="
9a630b795018bd6c142898fc49ab5a4866ebdd927f70fc1d687939e3e2d60629368a32903e44744213ec2ab48d66a2c25a10de8d6d7fcf3c097e9e366852469b  packer-1.8.1.tar.gz
"
