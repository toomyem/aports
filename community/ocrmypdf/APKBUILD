# Contributor: Carlo Landmeter <clandmeter@alpinelinux.org>
# Maintainer: Carlo Landmeter <clandmeter@alpinelinux.org>
pkgname=ocrmypdf
pkgver=13.4.7
pkgrel=0
pkgdesc="Add OCR text layer to scanned PDF files"
url="https://github.com/ocrmypdf/OCRmyPDF"
arch="noarch"
license="MIT"
options="!check" # missing pytest modules
depends="
	python3
	py3-cffi
	py3-chardet
	py3-coloredlogs
	py3-img2pdf
	py3-pdfminer
	py3-pikepdf
	py3-pillow
	py3-reportlab
	py3-tqdm

	ghostscript
	jbig2enc
	leptonica
	pngquant
	qpdf
	tesseract-ocr
	unpaper
	"
makedepends="py3-setuptools py3-setuptools_scm py3-setuptools-scm-git-archive"
checkdepends="py3-pytest py3-pytest-cov py3-pytest-xdist"
source="https://files.pythonhosted.org/packages/source/o/ocrmypdf/ocrmypdf-$pkgver.tar.gz"

prepare() {
	default_prepare
	sed -e '/setuptools_scm/d' \
		-e "/use_scm_version/cversion='$pkgver'," \
		-i setup.py
}

build() {
	python3 setup.py build
}

check() {
	python3 setup.py test
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="
bbe9e850d122142bec16b0201a32561c488e7a1f3029981336593abbdd18b495bee4e2b98aa6b2ba85646e99e3e308a3be2275dc00f678f0cc0b58a61341383b  ocrmypdf-13.4.7.tar.gz
"
